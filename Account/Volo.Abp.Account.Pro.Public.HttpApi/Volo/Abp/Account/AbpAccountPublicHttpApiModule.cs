﻿using Localization.Resources.AbpUi;
using Microsoft.Extensions.DependencyInjection;
using System;
using Volo.Abp.Account.Localization;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Identity;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;

namespace Volo.Abp.Account
{
	[DependsOn(
		typeof(AbpAccountPublicApplicationContractsModule),
		//typeof(AbpIdentityHttpApiModule),
		typeof(AbpAspNetCoreMvcModule)
	)]
	public class AbpAccountPublicHttpApiModule : AbpModule
	{
		public override void PreConfigureServices(ServiceConfigurationContext context)
		{
			base.PreConfigure<IMvcBuilder>(mvcBuilder =>
			{
				mvcBuilder.AddApplicationPartIfNotExists(typeof(AbpAccountPublicHttpApiModule).Assembly);
			});
		}

		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			base.Configure<AbpLocalizationOptions>(options =>
			{
				options.Resources.Get<AccountResource>().AddBaseTypes(new Type[]
				{
					typeof(AbpUiResource)
				});
			});
		}
	}
}
