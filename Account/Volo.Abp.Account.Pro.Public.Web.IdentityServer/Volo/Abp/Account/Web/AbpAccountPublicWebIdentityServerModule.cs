﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Account.Public.Web;
using Volo.Abp.Identity.AspNetCore;
using Volo.Abp.IdentityServer;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;

namespace Volo.Abp.Account.Web
{
	[DependsOn(
		typeof(AbpAccountPublicWebModule),
		typeof(AbpIdentityServerDomainModule)
	)]
	public class AbpAccountPublicWebIdentityServerModule : AbpModule
	{
		public override void PreConfigureServices(ServiceConfigurationContext context)
		{
			ServiceCollectionPreConfigureExtensions.PreConfigure<AbpIdentityAspNetCoreOptions>(context.Services, options =>
			{
				options.ConfigureAuthentication = false;
			});
			base.PreConfigure<IMvcBuilder>(mvcBuilder =>
			{
				mvcBuilder.AddApplicationPartIfNotExists(typeof(AbpAccountPublicWebIdentityServerModule).Assembly);
			});
		}

		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			base.Configure<AbpVirtualFileSystemOptions>(options =>
			{
				options.FileSets.AddEmbedded<AbpAccountPublicWebIdentityServerModule>();
			});
			IdentityCookieAuthenticationBuilderExtensions.AddIdentityCookies(AuthenticationServiceCollectionExtensions.AddAuthentication(context.Services, o =>
			{
				o.DefaultScheme = IdentityConstants.ApplicationScheme;
				o.DefaultSignInScheme = IdentityConstants.ExternalScheme;
			}));
		}
	}
}
