﻿using System.Threading.Tasks;
using Volo.Abp.AspNetCore.Mvc.UI.Theme.Shared.Toolbars;

namespace Volo.Abp.Account.Public.Web
{
	public class AccountModuleToolbarContributor : IToolbarContributor
	{
		public virtual Task ConfigureToolbarAsync(IToolbarConfigurationContext context)
		{
			//context.Toolbar.Name != "Main";
			return Task.CompletedTask;
		}

		public const int Order = 1000000;
	}
}
