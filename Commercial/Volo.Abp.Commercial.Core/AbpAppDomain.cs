﻿using System.Collections.Concurrent;
using System.Threading;

namespace Volo.Abp.Commercial.Core
{
    public static class AbpAppDomain
	{
		public static class License
		{
			public static readonly SemaphoreSlim OnlineLicenseCheckSemaphore;

			public static readonly object BypassMessageShowLocker;

			public static bool IsBypassMessageShown;

			public static ConcurrentDictionary<string, string> Lockers;

			static License()
			{
				OnlineLicenseCheckSemaphore = new SemaphoreSlim(1, 1);
				BypassMessageShowLocker = new object();
				IsBypassMessageShown = false;
				Lockers = new ConcurrentDictionary<string, string>();
			}
		}
	}
}
