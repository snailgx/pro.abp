﻿using System;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities.Events.Distributed;

namespace Volo.Payment.Requests
{
    [Serializable]
	public class PaymentRequestCompletedEto : EtoBase
	{
		public Guid Id { get; }

		public string Gateway { get; set; }

		public string Currency { get; set; }

		public List<PaymentRequestProductCompletedEto> Products { get; set; }

		public PaymentRequestCompletedEto(Guid id, string gateway, string currency, List<PaymentRequestProductCompletedEto> products)
		{
			this.Id = id;
			this.Gateway = gateway;
			this.Currency = this.Currency;
			this.Products = products;
		}
	}
}
