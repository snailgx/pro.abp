﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Modularity;

namespace Volo.Payment
{
	[DependsOn(
		typeof(AbpPaymentApplicationContractsModule),
		typeof(AbpAspNetCoreMvcModule)
	)]
	public class AbpPaymentHttpApiModule : AbpModule
	{
		public override void PreConfigureServices(ServiceConfigurationContext context)
		{
			base.PreConfigure<IMvcBuilder>(mvcBuilder =>
			{
				mvcBuilder.AddApplicationPartIfNotExists(typeof(AbpPaymentHttpApiModule).Assembly);
			});
		}
	}
}
