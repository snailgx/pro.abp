﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Application;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace Volo.Abp.TextTemplateManagement
{
	[DependsOn(
		typeof(TextTemplateManagementDomainModule),
		typeof(TextTemplateManagementApplicationContractsModule),
		typeof(AbpDddApplicationModule),
		typeof(AbpAutoMapperModule)
	)]
	public class TextTemplateManagementApplicationModule : AbpModule
	{
		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			context.Services.AddAutoMapperObjectMapper<TextTemplateManagementApplicationModule>();
			base.Configure<AbpAutoMapperOptions>(options =>
			{
				options.AddMaps<TextTemplateManagementApplicationModule>(true);
			});
		}
	}
}
