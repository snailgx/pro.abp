﻿namespace Volo.Abp.TextTemplateManagement
{
    public static class TextTemplateManagementDbProperties
	{
		public static string DbTablePrefix { get; set; }

		public static string DbSchema { get; set; }

		static TextTemplateManagementDbProperties()
		{
			TextTemplateManagementDbProperties.DbTablePrefix = "Abp";
			TextTemplateManagementDbProperties.DbSchema = null;
		}

		public const string ConnectionStringName = "TextTemplateManagement";
	}
}
