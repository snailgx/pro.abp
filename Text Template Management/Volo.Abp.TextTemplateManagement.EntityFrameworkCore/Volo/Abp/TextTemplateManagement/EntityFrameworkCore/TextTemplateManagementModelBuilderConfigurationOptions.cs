﻿using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Volo.Abp.TextTemplateManagement.EntityFrameworkCore
{
	public class TextTemplateManagementModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
	{
		public TextTemplateManagementModelBuilderConfigurationOptions(string tablePrefix = "", string schema = null)
			: base(tablePrefix, schema)
		{
		}
	}
}
