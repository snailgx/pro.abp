﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.AspNetCore.Mvc;

namespace Volo.Abp.TextTemplateManagement.TextTemplates
{
    [Area("textTemplateManagement")]
	[ControllerName("TextTemplateDefinitions")]
	[Authorize("TextTemplateManagement.TextTemplates")]
	[Route("api/text-template-management/template-definitions")]
	[RemoteService(true, Name = TextTemplateManagementRemoteServiceConsts.RemoteServiceName)]
	public class TemplateDefinitionController : AbpController, IRemoteService, IApplicationService, ITemplateDefinitionAppService
	{
		private readonly ITemplateDefinitionAppService _templateDefinitionAppService;

		public TemplateDefinitionController(ITemplateDefinitionAppService templateDefinitionAppService)
		{
			this._templateDefinitionAppService = templateDefinitionAppService;
		}

		[HttpGet]
		public virtual async Task<PagedResultDto<TemplateDefinitionDto>> GetListAsync(GetTemplateDefinitionListInput input)
		{
			return await this._templateDefinitionAppService.GetListAsync(input);
		}

		[Route("{name}")]
		[HttpGet]
		public virtual async Task<TemplateDefinitionDto> GetAsync(string name)
		{
			return await this._templateDefinitionAppService.GetAsync(name);
		}
	}
}
